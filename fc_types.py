import uuid
import typing
from typing import Union
from dataclasses import field, dataclass, Field

import FreeCAD
import FreeCADGui


class Area:
    def __new__(cls, value: Union[int, float, str]) -> FreeCAD.Units.Area:
        if isinstance(value, (int, float)):
            return FreeCAD.Units.Quantity(value, FreeCAD.Units.Area)
        if isinstance(value, str):
            return FreeCAD.Units.Quantity(value)


class FCUUID:
    def __new__(cls, value=None):
        if value:
            return str(value)
        return str(uuid.uuid4())


TYPES = {
    str: "App::PropertyString",
    int: "App::PropertyInteger",
    Area: "App::PropertyArea",
    FCUUID: "App::PropertyUUID",
}


class EasierFeaturePython:
    def __new__(cls, *args, **kwargs):
        if "doc" not in kwargs.keys():
            doc = FreeCAD.ActiveDocument
            if not doc:
                doc = FreeCAD.newDocument()
        obj = doc.addObject("Part::FeaturePython", cls.__name__)
        for key, type_hint in typing.get_type_hints(cls).items():
            fc_type = TYPES.get(type_hint, "App::PropertyString")
            obj.addProperty(fc_type, key)
            value = kwargs.get(key, None) or getattr(cls, key, None)
            if value:
                if isinstance(value, Field):
                    value = value.default_factory()
                value = value
                setattr(obj, key, value)
        for method_name in dir(cls):
            method = getattr(cls, method_name)
            if not callable(method) or method_name.startswith("__"):
                continue
            # Idea from : https://forum.freecadweb.org/viewtopic.php?t=3691
            # Warning. Read : https://stackoverflow.com/questions/972/adding-a-method-to-an-existing-object-instance
            setattr(obj, method_name, method)

        return obj


def main():
    pass

if __name__ == "__main__":
    main()