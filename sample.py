# encoding: UTF-8
from dataclasses import field

from fc_types import EasierFeaturePython, FCUUID, Area


class Custom(EasierFeaturePython):
    text: str = "I am a default text"
    area: Area = Area(1000000)
    my_uuid: FCUUID = field(default_factory=FCUUID)  # new uuid created for each instance

    def bound_method(self):
        print(f"I am a new bound method. I can read area : {self.area}")


def main():
    import FreeCAD
    import FreeCADGui

    FreeCADGui.showMainWindow()
    doc = FreeCAD.ActiveDocument or FreeCAD.newDocument()
    efp1 = Custom()
    efp2 = Custom(text="I am a modified text", area="5 m^2")
    FreeCADGui.exec_loop()


if __name__ == "__main__":
    main()
